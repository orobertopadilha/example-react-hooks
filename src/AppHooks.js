import { useEffect, useState } from "react"
import taskService from './service/TaskService'

const AppHooks = () => {
    const [tasks, setTasks] = useState(() => taskService.list())
    const [newTask, setNewTask] = useState('')

    useEffect(() => taskService.save(tasks), [tasks])

    const addTask = () => {
        setTasks([...tasks, newTask])
        setNewTask('')
    }

    return (
        <>
            <h1>Todo List - Com Hooks</h1>

            <input type="text" name="newTask" value={newTask}
                onChange={event => setNewTask(event.target.value)} />
            <input type="button" value="+" onClick={() => addTask()} />

            <ul>
                {
                    tasks.map((task, index) =>
                        <li key={index}>{task}</li>
                    )
                }
            </ul>
        </>
    )
}

export default AppHooks