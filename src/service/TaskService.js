const save = (tasks) => {
    localStorage.setItem('task-list', JSON.stringify(tasks))
}

const list = () => {
    let tempList = localStorage.getItem('task-list')
    if (tempList != null) {
        return JSON.parse(tempList)
    }

    return []
}

const taskService = { save, list }

export default taskService